<?php

namespace ForumCube\BDApi;

use ForumCube\BDApi\Helper\XfGeneral;
use Illuminate\Support\ServiceProvider;
use ForumCube\BDApi\Contracts\BdAuthProvider;
use Auth;
use Illuminate\Support\Facades\Event;

class BdapiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/bdconfig.php' => config_path('bdconfig.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        Auth::extend('bdauth', function ($app) {
            return new BdAuthProvider;
        });

        Auth::provider('bdauth', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            return new BdAuthProvider();
        });

        //Registering Events for login & logout
        Event::subscribe('ForumCube\BDApi\Listeners\UserEventSubscriber');

        //Publishing assets file
        $this->publishes([
            __DIR__ . '/assets' => public_path('vendor/forumcube/js'),
        ], 'public');


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/bdconfig.php', 'bdconfig'
        );

        $this->app->bind('xfgeneral', function ()
        {
            return new XfGeneral();
        });

        $this->app->singleton('forumcube.bdapi.console.kernel', function($app) {
            $dispatcher = $app->make(\Illuminate\Contracts\Events\Dispatcher::class);
            return new \ForumCube\BDApi\Console\Kernel($app, $dispatcher);
        });

        $this->app->make('forumcube.bdapi.console.kernel');
    }
}
