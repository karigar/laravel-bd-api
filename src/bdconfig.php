<?php

return [
    'xf_url'        =>  '',
    'bd_api_uri'    =>  '',
    'client_id'     =>  '',
    'client_secret' =>  '',
    'vendor_groups' =>  [2],
    'admin_groups'  =>  [3],
    'featured_usergroups'  =>  [4],
    'user_model'    => \ForumCube\BDApi\Model\User::class
];