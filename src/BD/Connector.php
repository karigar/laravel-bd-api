<?php
/**
 * Created by PhpStorm.
 * User: faisal
 * Date: 12/10/2016
 * Time: 12:31 AM
 */

namespace ForumCube\BDApi\BD;

use Dompdf\Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class Connector
{

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function request($target, array $params = [], $type = 'GET', $response_type = 'array') {
        // Create a client with a base URI
        $client = new Client(['base_uri' => xf_bd_url(), 'verify' => false]);

        $params['client_id'] = $this->config['client_id'];
        $params['client_secret'] = $this->config['client_secret'];

        $params = ($type == 'GET' || $type == 'DELETE') ? ['query' => $params] : ['form_params' => $params];

        try {
            $response = $client->request($type, $target, $params);
            return $this->parseResponse($response, $response_type);
        } catch (RequestException $exception) {
            return $this->processException($exception, $response_type);
        }  catch (\Exception $exception) {
//            Log::error($exception);
            return false;
            //return $this->processException($exception, $response_type);
        }
    }

    private function parseResponse($response, $type) {
        //If we got the successful response
        if($response->getStatusCode() == '200') {
            $content = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        }

        return $content;
    }

    private function processException($exception) {
        $content = \GuzzleHttp\json_decode($exception->getResponse()->getBody(true)->getContents(), true);

        Log::error($content);

//        if($exception->getResponse()->getStatusCode() == '401') {
//            $message = 'Wrong username / password combination';
//        }

//        if(isset($content['errors'])) {
//             $message = implode(', ', $content['errors']);
//        }

//        return $content;
        return false;
    }

}