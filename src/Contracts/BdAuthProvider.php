<?php

namespace ForumCube\BDApi\Contracts;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Session;
use ForumCube\BDApi\Facade\ConnectorFacade as Connector;
use ForumCube\BDApi\Contracts\XfUser;
use ForumCube\BDApi\Model\UserAccess;
use ForumCube\BDApi\Model\User;
use ForumCube\BDApi\Helper\ApiHelper;

class BdAuthProvider implements UserProvider
{

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $id
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveById($id)
    {

        $userModel = config('bdconfig.user_model');
        return $userModel::find($id);
    }

    /**
     * Retrieve a user by the given credentials.
     * DO NOT TEST PASSWORD HERE!
     *
     * @param  array  $credentials
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveByCredentials(array $credentials)
    {

        $credentials['grant_type'] = 'password';

        if(!isset($credentials['username']) && isset($credentials['email'])) {
            $credentials['username'] = $credentials['email'];
        }
        $user = Connector::request('oauth/token', $credentials, 'POST');

        if($user) {
            return new XfUser($user);
        }

        return ;
    }

//    private function userEntry($user = FALSE) {
//        //If we've the user we should check create it if not exists
//        $user_data = Connector::request('users/me', ['authorized' => $user]);
//        $userModel = config('bdconfig.user_model');
//
//        $xf_user = $user_data['user'];
//
//        if(isset($xf_user['user_email']) && !$xf_user['user_email']) {
//            return false;
//        }
//
//        //Storing XF user to session
//        session(['xf_user' => $xf_user]);
//        session(['xf_user_permissions' => $xf_user['global_permission_cache']]);
//
//        $xf_user['user_groups_ids'] = implode(",",array_column($xf_user['user_groups'],'user_group_id'));
//        $data = ['id' => $xf_user['user_id'], 'email' => $xf_user['user_email'], 'name' => $xf_user['username'], 'custom_fields' => $xf_user['fields'], 'user_groups' => $xf_user['user_groups'],'user_groups_ids' => $xf_user['user_groups_ids'], 'verified' => $xf_user['user_is_verified'] ,'created_at' => Carbon::createFromTimestamp($xf_user['user_register_date'])->toDateTimeString()];
//
//        $user_obj = $userModel::firstOrNew(['id'=> $xf_user['user_id']]);
//
//        $user_obj->fill($data)->save();
//
//        return $user_obj;
//    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Auth\UserInterface  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        //Create or Update user entry
        if(!ApiHelper::userEntry($user)) {
            return false;
        }

        return true;
    }


    /**
     * Needed by Laravel 4.1.26 and above
     */
    public function retrieveByToken($identifier, $token)
    {

        $params['grant_type'] = $identifier;
        $params['refresh_token'] = $token;

        $user = Connector::request('oauth/token', $params, 'POST');

        return $user;
    }

    /**
     * Needed by Laravel 4.1.26 and above
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        return new \Exception('not implemented');
    }
}