<?php

namespace ForumCube\BDApi\Contracts;

use Illuminate\Auth\GenericUser;

class XfUser extends GenericUser
{

    use \ForumCube\BDApi\Traits\XfUserTrait;

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'user_id';
    }


    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['password'];
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /*
     * Return array of attributes
     */
    public function toArray()
    {
        return $this->attributes;
    }
}
