<?php

namespace ForumCube\BDApi\Facade;

use Illuminate\Support\Facades\Facade as BaseFacade;
use ForumCube\BDApi\BD\Connector;
use Carbon\Carbon;
use ForumCube\BDApi\Model\UserAccess;
use Session;
use ForumCube\BDApi\Helper\ApiHelper;

class ConnectorFacade extends BaseFacade
{
    protected static $connection = false;

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'bd.api'; }

    public static function request($target, array $params = [], $type = 'GET', $response_type = 'array') {

        if(isset($params['authorized'])) {
            $params['oauth_token'] = self::authorizeRequest($params['authorized']);
            unset($params['authorized']);
        }

        $response = self::connection()->request($target, $params, $type, $response_type);

        //If response have access & refresh tokens we should update them
        if(isset($response['access_token']) && isset($response['refresh_token'])) {
            ApiHelper::updateAccess($response);
        }

        return $response;
    }

    public static function authorizeRequest($user = false)
    {

        if ($user === 'system') {
            $user_access = UserAccess::whereUserId(xf_system_user_id())->first();
            if ($user_access) {
                $user_access = $user_access->toArray();
            }
        } elseif (is_array($user) AND !isset($user['updated_at'])) {

            //If we don't have user access record as parameter, we should get it from session
            $user_access = UserAccess::whereAccessToken($user['access_token'])->first()->toArray();
        } elseif (!is_array($user)) {
            $user_access = session('user_access');
        } else {
            if ($user != 'system') {
                \Auth::logout();
            }
            return false;
        }

        //If Access token got 10 or more seconds to expire we're all good with this access token
        if (Carbon::now()->lte(Carbon::createFromTimestamp($user_access['updated_at'] + $user_access['access_token_expiry'] - 10))) {
            return $user_access['access_token'];
        }


        //Let's check if we have valid Refresh token & try to get new access token using that
        if (Carbon::now()->lte(Carbon::createFromTimestamp($user_access['updated_at'] + $user_access['refresh_token_expiry'] - 5))) {

            //Its valid so let's generate a new access token & use it in our next requests
            $params = [
                'grant_type' => 'refresh_token',
                'refresh_token' => $user_access['refresh_token']
            ];

            $user_access = self::request('oauth/token', $params, 'POST');

            if (isset($user_access['access_token']) && isset($user_access['access_token'])) {
                return $user_access['access_token'];
            }

        }

        if ($user != 'system') {
            //If both tokens are expired we should kick the user out of system
            \Auth::logout();
        }
        return false;
    }

    public static function connection() {

        if(!self::$connection)
            return self::$connection = new Connector(config('bdconfig'));

        return self::$connection;
    }
}