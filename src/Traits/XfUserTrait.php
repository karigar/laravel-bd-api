<?php

namespace ForumCube\BDApi\Traits;

use ForumCube\BDApi\Facade\ConnectorFacade as XfConnector;

trait XfUserTrait
{
    /*
     * Check if we have target Role
     *
     * param: $role (string|int) name or id of target role
     *
     * return: (boolean|array)
     */
    public function hasRole($role) {
        $checkValue = is_numeric($role) ? 'user_group_id' : 'user_group_title';

        foreach($this->user_groups as $user_group) {

            if($role == $user_group[$checkValue]) {
                return $user_group;
            }
        }

        return FALSE;
    }

    /*
     * This is extension for hasPrimaryOrSecondary & will allow you to check if user got target role as primary
     *
     * param: $role (string|int) name or id of target role
     *
     * return: (boolean|array)
     */
    public function hasPrimaryRole($role) {
        return $this->hasPrimaryOrSecondary($role);
    }

    /*
     * This method is extension for hasPrimaryOrSecondary & will check if user got target group as secondary
     */
    public function hasSecondaryRole($role) {
        return $this->hasPrimaryOrSecondary($role, false);
    }

    /*
     * This is helper method & you can check weather user got the specific role as primary or secondary.
     *
     */
    public function hasPrimaryOrSecondary($role, $primary = true) {
        $role = $this->hasRole($role);

        if($role && $primary && $role['is_primary_group']) {
            return $role;
        } elseif ($role && !$primary && !$role['is_primary_group']) {
            return $role;
        }

        return FALSE;
    }

    /*
     * Check any marketplace related permission for the user. This is extension for hasXFPermission method
     */
    public function hasPermission($permission, $userid = false) {

        if($this->isAdmin()) {
            return true;
        }

        //If both users are not same
        if($userid && $this->id != $userid) {
            return false;
        }

        return $this->hasXfPermission('marketplace_permissions.' . $permission);
    }

    /*
     *  This method will help to check the session for required permission checks from XF
     */
    public function hasXfPermission($permission, $userid = false) {
        return array_get(session('xf_user_permissions'), $permission);
    }

    /*
     * This will verify if user got required admin usergroup compared to the config file
     */
    public function isAdmin() {
        if(is_array(config('bdconfig.admin_groups'))) {
            foreach (config('bdconfig.admin_groups') as $adminRole) {
                if($validUser = $this->hasRole($adminRole)) {
                    return $validUser;
                }
            }
        }

        return false;
    }

    /*
     * This will verify if user got required vendor usergroup compared to the config file
     */
    public function isVendor() {

        //Pass on user if he's admin
        if($isAdmin = $this->isAdmin()) {
            return $isAdmin;
        }

        if(is_array(config('bdconfig.vendor_groups'))) {
            $validUser = false;
            foreach (config('bdconfig.vendor_groups') as $vendorRole) {
                if($validUser = $this->hasRole($vendorRole)) {
                    return $validUser;
                }
            }
        }

        return false;
    }

    public function isFeaturedVendor() {
        //Pass on user if he's admin
        if($isAdmin = $this->isAdmin()) {
            return $isAdmin;
        }

        if(is_array(config('bdconfig.featured_usergroups'))) {
            foreach (config('bdconfig.featured_usergroups') as $vendorRole) {
                if($validUser = $this->hasRole($vendorRole)) {
                    return $validUser;
                }
            }
        }

        return false;
    }

    public function getXfUserAttribute($target = false) {
        $response = XfConnector::request('users/' . $this->id, ['authorized' => true]);

        if(isset($response['user'])) {
            return $target ? $response['user'][$target] : $response['user'];
        }

        return false;
    }

    public function getAvatarAttribute($size = 'm') {
        $userId = $this->id;

        //@TODO: We can put a default fallback avatar below
        $avatar = '';
        if($userLinks = $this->getXfUserAttribute('links')) {
            switch ($size) {
                case 'small':
                case 's':
                    $avatar = $userLinks['avatar_small'];
                    break;
                case 'big':
                case 'large':
                case 'b':
                case 'l':
                    $avatar = $userLinks['avatar_big'];
                    break;

                default:
                    $avatar = $userLinks['avatar'];
                    break;
            }
        } else {
            $avatar = config('bdconfig.fallback_avatar');
        }

        return $avatar;
    }

    public function userAccess() {
        return $this->hasOne('ForumCube\BDApi\Model\UserAccess','user_id');
    }

}