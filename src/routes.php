<?php


Route::get('/auth/token', 'ForumCube\BDApi\Controllers\Login@token')->name('auth-token');
Route::get('/auth/apilogout', 'ForumCube\BDApi\Controllers\Login@logout')->name('auth-logout');

Route::match(array('GET','POST'),'/xf-connect', ['as' => 'xf-connect', 'uses' => 'ForumCube\BDApi\Controllers\Login@connectAdminXf']);
Route::match(array('GET'),'/xf-disconnect', ['as' => 'xf-disconnect', 'uses' => 'ForumCube\BDApi\Controllers\Login@disConnectAdminXf']);

