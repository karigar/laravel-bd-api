<?php

namespace ForumCube\BDApi\Listeners;

use Auth;
use Request;
use ForumCube\BDApi\Helper\ApiHelper;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {
        //Create a user entry of new logged in user
        ApiHelper::userEntry();

        if(!Request::ajax() AND Auth::check() AND !isset(Request::all()['xfac'])){
            redirect(ApiHelper::loginXFUrl())->send();
        }
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event) {
        if(Auth::check() AND !Request::ajax() AND !Request::get('jsLogout')) {
            redirect(ApiHelper::logoutXFUrl())->send();
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'ForumCube\BDApi\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'ForumCube\BDApi\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}