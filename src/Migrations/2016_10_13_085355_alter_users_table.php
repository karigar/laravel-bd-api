<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->text('custom_fields');
            $table->text('user_groups');
            $table->string('user_groups_ids');
            $table->tinyInteger('verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('custom_fields');
            $table->dropColumn('user_groups');
            $table->dropColumn('user_groups_ids');
            $table->string('password');
        });
    }
}
