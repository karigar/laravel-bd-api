<?php

namespace ForumCube\BDApi\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use \ForumCube\BDApi\Traits\XfUserTrait,Authenticatable, Authorizable, CanResetPassword;


    protected $table = 'users';
    protected $fillable = ['id', 'email', 'name', 'custom_fields', 'user_groups', 'user_groups_ids', 'created_at', 'verified'];
    protected $casts = [
        'custom_fields' => 'array',
        'user_groups' => 'array'
    ];

    protected $dates = [
        'created_at'
    ];

}