<?php

namespace ForumCube\BDApi\Model;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    protected $table = 'users_access';
    protected $dateFormat = 'U';
    protected $fillable = ['user_id', 'access_token', 'access_token_expiry', 'refresh_token', 'refresh_token_expiry'];


}
