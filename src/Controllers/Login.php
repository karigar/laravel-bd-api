<?php

namespace ForumCube\BDApi\Controllers;

use ForumCube\BDApi\Model\UserAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ForumCube\BDApi\Facade\ConnectorFacade as Connector;
use ForumCube\BDApi\Helper\ApiHelper;
use Auth;


class Login extends Controller
{
    public function token() {
        if (empty($_REQUEST['xfac'])) {
            return;
        }

        $loginUrl = route('auth-token');
        $redirectTo = $_REQUEST['redirect_to'];
        $redirectToRequested = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '';

        $redirectBaseUrl = $loginUrl . (strpos($loginUrl, '?') !== false ? '&' : '?') . 'redirect_to=' . urlencode($redirectTo);

        $callbackUrl = $redirectBaseUrl . '&xfac=callback';

        $token = false;
        $associateConfirmed = false;

        switch ($_REQUEST['xfac']) {
            case 'callback':
                define('XFAC_SYNC_LOGIN_SKIP_REDIRECT', 1);

                if (!empty($_REQUEST['code'])) {
                    $token = ApiHelper::getAccessTokenFromCode($_REQUEST['code'], $callbackUrl);
                }
                break;
            case 'authorize':
            default:
                $scope = '';
                if (!empty($_REQUEST['admin'])) {
                    $scope = XFAC_API_SCOPE . ' admincp';
                }

                if ($_REQUEST['xfac'] === 'authorize') {
                    // user is requesting to connect their own account
                    // include a hash to skip the associate submission if possible
                    $callbackUrl .= '&authorizeHash=' . urlencode(_xfac_login_getAuthorizeHash());
                }

                $authorizeUrl = call_user_func_array('sprintf', array(
                    '%s?oauth/authorize/&client_id=%s&redirect_uri=%s&response_type=code&scope=%s',
                    rtrim(xf_bd_url(), '/'),
                    rawurlencode(config('bdconfig.client_id')),
                    rawurlencode($callbackUrl),
                    rawurlencode($scope ? $scope : 'read post conversate'),
                ));

                // wp_redirect($authorizeUrl);
                // cannot use wp_redirect because wp_sanitize_redirect changes our url
                // issues: it removes basic auth (http://user:password@path)
                // TODO: find better way to do this
                header("Location: $authorizeUrl", true, 302);
                exit();
        }

        if (empty($token)) {
            dd('No token from XF');
        }
        if (empty($token['scope'])) {
            dd('No scope in URL');
        }

        $me = Connector::request('users/me', ['oauth_token' => $token['access_token']]);

        if (empty($me['user'])) {
            dd('error getting user via token');
        }
        $xfUser = $me['user'];

        //Create or Update user entry
        ApiHelper::userEntry($token);

        Auth::loginUsingId($xfUser['user_id'], true);

        //redirecting back user to orignal url
        return redirect($redirectToRequested);
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }


    public function connectAdminXf(Request $request) {

        if(!Auth::check() OR (Auth::check() AND !Auth::user()->isAdmin())) {
            abort(403, 'Unauthorized action.');
        }

        if($request->isMethod('post')) {
            if(!isset($credentials['username']) && isset($credentials['email'])) {
                $credentials['username'] = $credentials['email'];
            }
            $credentials = [
                'grant_type'    => 'password',
                'username'  =>  xf_system_user(),
                'password'  =>  $request->password
            ];

            $user = Connector::request('oauth/token', $credentials, 'POST');

            if(isset($user['access_token'])) {
                if($request->ajax()) {
                    return response()->json([
                        'message' => 'Admin account connected successfully',
                        'redirectUrl'   =>  'back'
                    ]);
                }
                return redirect(route('home'));
            }
        }

        if($request->ajax()) {
            return response()->json(['responseText' => ['Not able to login']], 403);
        }
        return redirect(route('home'));
    }

    public function disConnectAdminXf() {

        if(!Auth::check() OR (Auth::check() AND !Auth::user()->isAdmin())) {
            abort(403, 'Unauthorized action.');
        }

        if($user = UserAccess::whereUserId(xf_system_user_id())->first()) {
            $user->delete();
        }
        
        return redirect(route('home'));
    }

}
