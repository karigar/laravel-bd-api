<?php

if (! function_exists('xf_base_url')) {
    function xf_base_url() {
        return config('bdconfig.xf_url');
    }
}

if (! function_exists('xf_bd_url')) {
    function xf_bd_url() {
        return xf_base_url() . config('bdconfig.bd_api_uri');
    }
}

if (! function_exists('xf_url')) {
    function xf_url($uri = '', $params = '') {
        if(is_array($params)) {
            $params = '?' . http_build_query($params);
        }
        return xf_base_url() . ltrim($uri,'/') . $params;
    }
}

if (! function_exists('xf_system_user')) {
    function xf_system_user() {
        return config('bdconfig.xf_system_user');
    }
}

if (! function_exists('xf_system_user_id')) {
    function xf_system_user_id() {
        return config('bdconfig.xf_system_user_id');
    }
}

if (! function_exists('bd_user_modal')) {
    function bd_user_modal()
    {
        return config('bdconfig.user_model');
    }
}

if (! function_exists('xf_system_user_alive')) {
    function xf_system_user_alive()
    {
        return \ForumCube\BDApi\Helper\ApiHelper::xfSystemUserAlive();
    }
}