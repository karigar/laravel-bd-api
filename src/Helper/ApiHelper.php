<?php

namespace ForumCube\BDApi\Helper;

use Request;
use Auth;
use ForumCube\BDApi\Facade\ConnectorFacade as Connector;
use Carbon\Carbon;
use ForumCube\BDApi\Model\UserAccess;
use Session;
use Illuminate\Support\Facades\Log;


class ApiHelper
{
    public static function loginXFUrl() {
        $redirectUri = self::redirectUrl();

        return self::SyncXFUrls($redirectUri);
    }

    public static function logoutXFUrl() {
        return self::SyncXFUrls(url('/'), 'logout');
    }

    protected static function SyncXFUrls($redirectUri, $operation = 'login') {
        $userid = isset(Auth::user()->user_id) ? Auth::user()->user_id : Auth::user()->id;
        $accessToken = isset(Auth::user()->access_token) ? Auth::user()->access_token : Auth::user()->userAccess->access_token;

        $ott = self::generateOneTimeToken($userid, $accessToken);
        return sprintf('%s?tools/%s&oauth_token=%s&redirect_uri=%s',
            xf_bd_url(),
            $operation,
            rawurlencode($ott),
            rawurlencode($redirectUri)
        );
    }

    protected static function redirectUrl() {
        $redirectUri = Request::get('return') ? Request::fullUrl() : Request::url();

        if(Request::get('redirect_to')) {
            $redirectUri = Request::get('redirect_to');
        }

        return $redirectUri;

    }

    protected static function generateOneTimeToken($userId = 0, $accessToken = '', $ttl = 10)
    {
        $timestamp = time() + $ttl;
        $once = md5($userId . $timestamp . $accessToken . config('bdconfig.client_secret'));

        return sprintf('%d,%d,%s,%s', $userId, $timestamp, $once, config('bdconfig.client_id'));
    }

    public static function getAccessTokenFromCode($code, $redirectUri)
    {
        $url = call_user_func_array('sprintf', array(
            '%s?oauth/token/',
            xf_bd_url()
        ));

        $postFields = array(
            'grant_type' => 'authorization_code',
            'client_id' => config('bdconfig.client_id'),
            'client_secret' => config('bdconfig.client_secret'),
            'code' => $code,
            'redirect_uri' => $redirectUri,
        );

        $user_access = Connector::request('oauth/token', $postFields, 'POST');
        return self::prepareAccessTokenBody($user_access);
    }


    protected static function prepareAccessTokenBody($parts)
    {
        if (!empty($parts['access_token'])) {
            if (!empty($parts['expires_in'])) {
                $parts['expire_date'] = time() + $parts['expires_in'];
                unset($parts['expires_in']);
            }

            if (!empty($parts['refresh_token_expires_in'])) {
                $parts['refresh_token_expire_date'] = time() + $parts['refresh_token_expires_in'];
                unset($parts['refresh_token_expires_in']);
            }

            foreach (array_keys($parts) as $key) {
                if (is_array($parts[$key])) {
                    unset($parts[$key]);
                }
            }

            return $parts;
        } else {
//            return _xfac_api_getFailedResponse($parts);
        }
    }

    public static function userEntry($user = FALSE) {
        //If we've the user we should check create it if not exists
        $user_data = Connector::request('users/me', ['authorized' => $user]);
        $userModel = config('bdconfig.user_model');

        $xf_user = $user_data['user'];

        if(isset($xf_user['user_email']) && !$xf_user['user_email']) {
            Log::error(['error' => 'Following User not have email associated'] + $xf_user);
            abort(403, 'No email setup');
            return false;
        }

        //Storing XF user to session
        session(['xf_user' => $xf_user]);
        session(['xf_user_permissions' => $xf_user['global_permission_cache']]);

        $xf_usergroup_ids = '';
        if($xf_user['user_groups']) {
            $xf_usergroup_ids = implode(',' , array_pluck($xf_user['user_groups'], 'user_group_id'));
        }

        $data = ['id' => $xf_user['user_id'], 'email' => $xf_user['user_email'], 'name' => $xf_user['username'], 'custom_fields' => isset($xf_user['fields'])? $xf_user['fields']:[], 'user_groups' => $xf_user['user_groups'],'user_groups_ids' => $xf_usergroup_ids, 'verified' => $xf_user['user_is_verified'] ,'created_at' => Carbon::createFromTimestamp($xf_user['user_register_date'])->toDateTimeString()];

        $user_obj = $userModel::firstOrNew(['id'=> $xf_user['user_id']]);

        $user_obj->fill($data)->save();

        return $user_obj;
    }

    public static function loginScript() {
        $html = '<script>';

        $html .= 'window.xfacClientId = "' . config('bdconfig.client_id') . '"; ';
        if(Auth::check()) {
            $html .= 'window.xfacWpLogout = "' . route('auth-logout') . '?jsLogout=1"; ';
            $html .= 'window.xfacXenForoUserId = ' . Auth::user()->id . '';
        } else {
            $html .= 'window.xfacWpLogin = "' . route('auth-token') . '?xfac=1"; ';
        }

        $html .= '</script>';

        //Scripts to make this work
        $html .= "<script type='text/javascript' src='" . xf_bd_url() . "?assets/sdk&prefix=xfac'></script>
                    <script type='text/javascript' src='" . asset('vendor/forumcube/js/login.js') . "'></script>";

        return $html;
    }

    /*
     * This method will allow the session & db to update user's access info
     */
    public static function updateAccess(array $xfUser) {
        //Update new access in session & db
        $data = [
            'access_token'          =>  $xfUser['access_token'],
            'access_token_expiry'   =>  $xfUser['expires_in'],
            'refresh_token'         =>  $xfUser['refresh_token'],
            'refresh_token_expiry'  =>  $xfUser['refresh_token_expires_in'],
            'user_id'               =>  $xfUser['user_id']
        ];

        //Store this to db
        $user_access = UserAccess::firstOrNew(['user_id'=> $data['user_id']]);

        $user_access->fill($data)->save();


        //We don't need to store this in session if its a system user
        if($data['user_id'] != xf_system_user_id()) {
            Session::put('user_access', $user_access->toArray());
            Session::save();
        }

        return $user_access;
    }

    /*
     *  This method will return list of XF usergroups
     */
    public static function xfUserGroups() {
        return self::request('users/groups', ['authorized' => false]);
    }

    /*
     * Check if system user is connected
     */
    public static function xfSystemUserAlive() {
        $user_data = Connector::request('users/me', ['authorized' => 'system']);
        if(isset($user_data['user']) AND $user_data['user']['username'] == xf_system_user()) {
           return true;
        }

        return false;
    }
}